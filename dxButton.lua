-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxButton.lua
-- PROPOSITO: Libreria de Botones
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

Button = {}

function dxButton ( text, x, y, w, h, textura, fuente, color_enfasis, postgui )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
  assert( type( text ) == "string", "Mal argumento @ dxButton" );
	assert( type( x ) == "number", "Mal argumento @ dxButton" );
	assert( type( y ) == "number", "Mal argumento @ dxButton" );
  assert( type( w ) == "number", "Mal argumento @ dxButton" );
	assert( type( h ) == "number", "Mal argumento @ dxButton" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxButton' )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'text', text )
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'w', w )
  self:setData ( 'h', h )
  self:setData ( 'visible', true )
  self:setData ( 'textura', textura )
  self:setData ( 'font', fuente or 'default' )
  self:setData ( 'font_size', 1 )
  self:setData ( 'color', (textura) and 0xBBAAAAAA or 0xAA000000 )
  self:setData ( 'colorEnfasis', color_enfasis or (textura) and 0xBBFFFFFF or 0xAA444444 )
  self:setData ( 'source', sourceResourceRoot )
  
  if postgui and type( postgui ) ~= 'boolean' then

    -- Revisa si el valor 'text' fue entregado correctamente
    outputDebugString ( "Mal argumento 'postgui' @ dxEdit", 2 )
    postgui = nil
  
  end
  
  self:setData ( 'postgui', postgui )
  
  return self
  
end

function Button.render ( v, x, y, w, h, dxtarget )

  local text = v:getData ( 'text' )
  local textura = v:getData('textura')
  local fuente = v:getData('font')
  local color = v:getData('color')
  local color_enfasis = v:getData('colorEnfasis')
  local font_size = v:getData('font_size')
  local text_offset = v:getData('text_offset') or 0
  local postgui = v:getData('postgui') or false
  local disabled = v:getData( 'disabled' )
  
  if textura then
    local color = ( isCursorInPosition ( x, y, w, h, dxtarget ) and not disabled ) and color_enfasis or color 
    dxDrawImage ( x, y, w, h, textura,0,0,0,color, postgui )
  else
    local color = ( isCursorInPosition ( x, y, w, h, dxtarget ) and not disabled ) and color_enfasis or color 
    dxDrawRectangle ( x, y, w, h, color, postgui )
  end
  dxDrawText ( text, x, y, w + x + text_offset, h + y, nil, font_size, fuente, 'center', 'center', true, false, postgui )
  
end
