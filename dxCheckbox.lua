-- ****************************************************************************
--
-- RECURSO: dxCheckbox
-- ARCHIVO: dxCheckbox.lua
-- PROPOSITO: Libreria de Checkbox
-- CREADORES: Federico Romero < TheCrazy17 >
--
-- ****************************************************************************

Checkbox = {}

function dxCheckbox(x, y, w, h, state)
	-- Revisa que las funciones requeridas se hayan entregado correctamente
	assert( type( x ) == "number", "Mal argumento @ dxBox" );
	assert( type( y ) == "number", "Mal argumento @ dxBox" );
	assert( type( w ) == "number", "Mal argumento @ dxBox" );
	assert( type( h ) == "number", "Mal argumento @ dxBox" );

	-- Crea el elemento del checkbox
	local self = Element('dxCheckbox')
  
	if not self then return false end

	-- Agrega los datos requeridos al elemento
	self:setData('x', x)
	self:setData('y', y)
	self:setData('w', w)
	self:setData('h', h)
	self:setData('visible', true)
	self:setData('source', sourceResourceRoot)
	self:setData('state', state or false)

	return self
end

function Checkbox.Render(v, x, y, w, h)
	dxDrawRectangle(x, y, w, h, 0xDD000000)

	dxDrawLine(x, y, x + w, y, 0xDDFFFFFF)
	dxDrawLine(x, y + h, x + w, y + h, 0xDDFFFFFF)
	dxDrawLine(x, y, x, y + h, 0xDDFFFFFF)
	dxDrawLine(x + w, y, x + w, y + h, 0xDDFFFFFF)

	if v:getData('state') then
		dxDrawLine(x, y, x + w, y + h, 0xDD2EFE9A)
		dxDrawLine(x, y + h, x + w, y, 0xDD2EFE9A)
	end
end
