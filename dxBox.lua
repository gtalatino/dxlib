-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxBox.lua
-- PROPOSITO: Libreria de Cajas
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

Box = {}

function dxBox ( x, y, w, h, color, postgui, subpixel )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
	assert( type( x ) == "number", "Mal argumento @ dxBox" );
	assert( type( y ) == "number", "Mal argumento @ dxBox" );
  assert( type( w ) == "number", "Mal argumento @ dxBox" );
	assert( type( h ) == "number", "Mal argumento @ dxBox" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxBox' )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'w', w )
  self:setData ( 'h', h )
  self:setData ( 'visible', true )
  self:setData ( 'source', sourceResourceRoot )
  
  if color then

    -- Revisa si el valor 'color' fue entregado correctamente
    if type( color ) ~= 'number' and type( color ) ~= 'string' then
  
      outputDebugString ( "Mal argumento 'color' @ dxBox", 2 )
  
    else
  
      self:setData ( 'color', color )
  
    end
    
  end
  
  if postgui then

    if type ( postgui ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'postgui' @ dxBox", 2 )
  
    else
  
      self:setData ( 'postgui', postgui )
  
    end
  
  end
  
  if subpixel then

    if type ( subpixel ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'subpixel' @ dxBox", 2 )
  
    else
  
      self:setData ( 'subpixel', subpixel )
  
    end
  
  end
  
  return self
  
end

function Box.render ( v, x, y, w, h )

  local color = v:getData ( 'color' ) or nil
  local postgui = v:getData ( 'postgui' ) or nil
  local subpixel = v:getData ( 'subpixel' ) or nil
  
  dxDrawRectangle ( x, y, w, h, color, postgui, subpixel )

end
