-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxLine.lua
-- PROPOSITO: Libreria de Lineas
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

Line = {}

function dxLine ( x, y, x2, y2, color, width, postgui )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
	assert( type( x ) == "number", "Mal argumento x @ dxLine" );
	assert( type( y ) == "number", "Mal argumento y @ dxLine" );
  assert( type( x2 ) == "number", "Mal argumento x2 @ dxLine" );
	assert( type( y2 ) == "number", "Mal argumento y2 @ dxLine" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxLine' )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'x2', x2 )
  self:setData ( 'y2', y2 )
  self:setData ( 'visible', true )
  self:setData ( 'source', sourceResourceRoot )
  
  if color then

    -- Revisa si el valor 'color' fue entregado correctamente
    if type( color ) ~= 'number' and type( color ) ~= 'string' then
  
      outputDebugString ( "Mal argumento 'color' @ dxLine", 2 )
      self:setData ( 'color', 0xFFFFFFFF )
  
    else
  
      self:setData ( 'color', color )
  
    end
    
  end
  
  if width then

    if type ( width ) ~= 'number' then
  
      outputDebugString ( "Mal argumento 'width' @ dxLine", 2 )
      self:setData ( 'width', 1 )
  
    else
  
      self:setData ( 'width', width )
  
    end
  
  end
  
  if postgui then

    if type ( postgui ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'postgui' @ dxLine", 2 )
      self:setData ( 'postgui', false )
  
    else
  
      self:setData ( 'postgui', postgui )
  
    end
  
  end
  
  return self
  
end

function Line.render ( v, x, y )

  local x2 = v:getData ( 'x2' )
  local y2 = v:getData ( 'y2' )
  local color = v:getData ( 'color' )
  local postgui = v:getData ( 'postgui' )
  local width = v:getData ( 'width' )
  
  dxDrawLine ( x, y, x2, y2, color, width, postgui )

end
