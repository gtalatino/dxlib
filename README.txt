-------------------------------------------------------
-/
-
-           DXLIB POR GOTHEM Y TheCrazy
-
-\
--------------------------------------------------------
-/
- ACERCA DE:

  Este recurso contiene funciones utiles para el trabajo con graficos DirectX, especialmente en el trabajo de textos y imagenes.
  
  El trabajo expuesto aqui fue realizado en conjunto por Gothem y TheCrazy tras meses de desarrollo y analisis de los distintos
  recursos de librerias ya disponibles. ( Cabe destacar que partes del codigo pueden ser muy similares a otros recursos ya que se usaron como base )
  
-\
--------------------------------------------------------
-/
- FUNCIONES:

  NOTA: Las funciones descritas a continuación se definen de manera similar a como se definen en la Wiki de MTA, esto es, el nombre
    de la función, seguido del tipo de variables y nombre de variables. Posterior a esto se definen de manera mas completa las
    variables y finalmente con el signo '=' se define lo que devuelve la función.

  NOTA2: A todos los elementos creados por estas funciones se les pueden cambiar las variables mediante setElementData o por su variante OOP.
  
  NOTA3: El signo '*' define variables que se pueden cambiar mediante setElementData.

  NOTA4: Todos los elementos tienen la variable 'visible' que permite esconderlos.

-----
    
  dxText ( string texto, int x, int y, int w, int h [ , int color, float scale, string font, string alignx, string aligny, bool clip, bool wordbreak,
    bool postgui, bool colorcoded, bool subpixel, float rot, float rotcenterx, float rotcentery ] )
  
    - texto: Texto a escribir.
    - X: Posición en X del texto
    - Y: Posición en Y del texto
    - w: Ancho del espacio para el texto.
    - h: Alto del espacio para el texto.
    - color: Codigo del color del texto. ( En Hexadecimal o obtenido con la función toColor )
    - scale: El tamaño del texto a dibujar.
    - font: Fuente ( Estilo ) del texto.
    - alignx: Alineación en el eje X del texto.
    - aligny: Alineación en el eje Y del texto.
    - clip: Indica si se recorta o no el texto que esta fuera del espacio designado.
    - wordbreak: Indica si se rompe o no las lineas al exceder el ancho designado.
    - postgui: Indica si se pone por sobre o bajo de una GUI dibujada ( por CEGUI )
    - colorcoded: Indica si se toma en cuenta los codigos de color que pueda tener el texto.
    - subpixel: Indica si se renderiza de manera que sea mas suavizado el dibujado del texto.
    - rot: Rotación del texto.
    - rotcenterx: Centro de giro en el eje X
    - rotcentery: Centro de giro en el eje Y.
    
    = dxElement/dxText: Elemento de MTA que indica que el texto se a creado.
    
-----

  dxBox ( int x, int y, int w, int h, int color [ , bool postgui, bool subpixel ] )
  
    - X: Posición en X de la caja
    - Y: Posición en Y de la caja
    - w: Ancho de la caja.
    - h: Alto de la caja.
    - color: Codigo del color de la caja. ( En Hexadecimal o obtenido con la función toColor )
    - postgui: Indica si se pone por sobre o bajo de una GUI dibujada ( por CEGUI )
    - subpixel: Indica si se renderiza de manera que sea mas suavizado el dibujado de la caja.
     
    = dxElement/dxBox: Elemento de MTA que indica que la caja se a creado.

-----

  dxButton ( string texto, int x, int y, int w, int h[, mixed textura, string font, int/hex color_enfasis ] )
  
    - texto: Texto a escribir en el botón.
    - X: Posición en X del boton
    - Y: Posición en Y del boton
    - w: Ancho del boton
    - h: Alto del boton
    - textura: Permite poner una imagen de fondo en el boton ( puede ser una textura o una dirección )
    - font: Fuente con la cual se muestra el texto del boton.
    - color_enfasis: Color que toma el boton al pasar el mouse sobre este.
     
    = dxElement/dxButton: Elemento de MTA que indica que el boton se a creado.

    * font_size: Tamaño de la fuenta del texto.
    * color: Color del botón.
    * text_offset: Desplazamiento en el eje X del texto.

-----

  dxEdit ( int x, int y, int w, int h [ , string text ] )
  
    - X: Posición en X del campo de edición
    - Y: Posición en Y del campo de edición
    - w: Ancho del campo de edición
    - h: Alto del campo de edición
    - text: Texto inicial del campo de edición
     
    = dxElement/dxButton: Elemento de MTA que indica que el campo de edición se a creado.

    * mask: Bool, permite esconder el texto al usuario final.

-----

  setFocusedEdit ( element/dxEdit elemento, bool estado )
  
    - elemento: Campo de edición a enfocar.
    - estado: Indica si se enfoca o no el campo de edición.
    
    = bool: Devuelve verdadero si se cambio correctamente el enfoque.

-----

  dxSwitch ( int x, int y, int w, int h, bool state, float font_size )
  
    - X: Posición en X del interruptor
    - Y: Posición en Y del interruptor
    - w: Ancho del interruptor
    - h: Alto del interruptor
    - state: Indica el estado del interruptor.
    - font_size: Tamaño de la fuente en el switch.
     
    = dxElement/dxSwitch: Elemento de MTA que indica que el interruptor se a creado.
    
-----

  dxGridList ( int x, int y, int w, int h [ , int color ] )
  
    - X: Posición en X de la lista
    - Y: Posición en Y de la lista
    - w: Ancho de la lista
    - h: Alto de la lista
    - color: Color de fondo de la lista.
     
    = dxElement/dxGridList: Elemento de MTA que indica que la lista se a creado.
    
-----

  dxSwitchSetState ( element/dxSwitch self, bool state )
  
    - self: Interruptor a modificar.
    - state: Indica el estado en que se dejara el interruptor.
    
    = bool: Devuelve verdadero si se cambio correctamente el estado.
    
-----

  dxGridListAddColumn ( element/dxGridList self, string nombre [, int size ] )
  
    - self: Lista a modificar.
    - nombre: Nombre de la columna a agregar.
    - size: Ancho de la columna.
    
    = int: Numero de columna. ( falso si es que no se pudo agregar )
    
-----

  dxGridListAddRow ( element/dxGridList self )
  
    - self: Lista a modificar.
    
    = int: Numero de fila ( falso si es que no se pudo agregar )
    
-----

  dxGridListSetItemText ( element/dxGridList self, int rowid, int columnid, string text )
  
    - self: Lista a modificar.
    - rowid: Numero de fila del item.
    - columnid: Numero de columna de item.
    - text: Texto a agregar.
    
    = bool: Devuelve verdadero si se cambio correctamente el estado.
    
-----

  dxGridListSetSelectedItem ( Element/dxGridList self, int rowid, int columnid )
  
    - self: Lista a modificar.
    - rowid: Numero de fila del item.
    - columnid: Numero de columna de item.
    
    = bool: Devuelve verdadero si se cambio correctamente el estado.
    
-----

  dxGridListGetSelectedItem ( Element/dxGridList self )
  
    - self: Lista a evaluar.
    
    = bool: Devuelve el item seleccionado de la lista.

-----

  dxImage ( int x, int y, int w, int h, mixed image [, int/hex color ] )
  
    - X: Posición en X de la imagen.
    - Y: Posición en Y de la imagen.
    - w: Ancho de la imagen.
    - h: Alto de la imagen
    - image: Ruta de imagen o textura o shader.
    - color: Color de la imagen
 
    = dxElement/dxImage: Elemento de MTA que indica que la imagen se a creado.
    
-----

  dxWindow ( int x, int y, int w, int h [, string titulo ] )
  
    - X: Posición en X de la imagen.
    - Y: Posición en Y de la imagen.
    - w: Ancho de la imagen.
    - h: Alto de la imagen
    - titulo: Titulo de la ventana
 
    = dxElement/dxWindow: Elemento de MTA que indica que la ventana se a creado.

-
-\
----------------------------------------------------------------------------------------------------
-/
- EVENTOS:

  NOTA: Los eventos descritos acontinuación se definiran como el nombre del evento al inicio,
    seguido por el elemento source ( indicados por '=' ) y los parametros del evento ( indicado por '-' )

-----

  dxClick
  
    = dxElement: Todo elemento creado por el recurso.
    
    - button: Boton del ratón presionado.
    
-----

  dxCursorEnter
  
    = dxElement: Todo elemento creado por el recurso.
    
    - cx: Coordenada X del cursor.
    - cy: Coordenada Y del cursor.
    
-----

  dxCursorLeave
  
    = dxElement: Todo elemento creado por el recurso.
    
    - cx: Coordenada X del cursor.
    - cy: Coordenada Y del cursor.
    
-----

  dxEditChange
  
    = dxEdit: El campo de edición modificado.
    
    - new_text: El texto nuevo que se va a agregar
    
    * NOTA: ESTE EVENTO PUEDE SER CANCELADO, PREVINIENDO EL CAMBIO DEL TEXTO.
    
-
-\
---------------------------------------------------------------------------------------------------------
-/
- CONTACTO:

  Cualquier duda o problema con el recurso puedes contactarnos mediante mensaje privado enviado por el foro de GTA.La a Gothem o TheCrazy.
  Tambien nos puedes contactar en el foro de MTA oficial, buscando al usuario gothem.
  Por otro lado tambien puedes reportar problemas o contactarnos mediante el repositorio del recurso: https://bitbucket.org/Gothem/dxlib
  
  Esperamos que le hagas un buen uso a este recurso y crees cosas nunca antes vistas entregando los correspondientes creditos.
  