-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: render.lua
-- PROPOSITO: Renderizado de funciones
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

function render()
  for k,v in ipairs ( getElementsByType('texture', resourceRoot ) ) do
    dxSetRenderTarget ( v, true )
    dxSetRenderTarget ()
  
  end

	for k,v in ipairs(DYNAMICROOT:getChildren()) do
		if isElement(v) and v.type:find('dx') then
			renderizarElemento(v)
		end
	end
end
addEventHandler ( 'onClientRender', root, render )

function renderizarElemento(v, px, py, pw, ph)
	if v:getData('visible') then
		local x = v:getData ( 'x' )
		local y = v:getData ( 'y' )
		local w = v:getData ( 'w' ) or 0
		local h = v:getData ( 'h' ) or 0
    local dxtarget = v:getData ( 'dxtarget' ) or false

    if dxtarget then
  
      dxSetRenderTarget ( dxtarget )
  
    end
    
		-- Previene que el elemento exceda el ancho del parentesco.
		if pw and w > pw - x then
			w = pw - x
			v:setData ( 'w', w )
			outputDebugString ( "Ancho cambiado @ render" )
		end
    
    -- Previene que el elemento exceda la altura del parentesco.
    if ph and h > ph - y then
    
      h = ph - y
      v:setData ( 'h', h )
      outputDebugString ( "Altura cambiada @ render" )
    
    end
    
    -- Actualiza la posición de acuerdo a la posición del parentesco.
    x = x + ( px or 0 )
    y = y + ( py or 0 )
  
    -- Renderiza según el tipo de elemento.
    if v.type == 'dxText' then
    
      Text.render ( v, x, y, w, h )
      
    elseif v.type == 'dxBox' then
    
      Box.render ( v, x, y, w, h )
    
    elseif v.type == 'dxButton' then
    
      Button.render ( v, x, y, w, h, dxtarget )
    
    elseif v.type == 'dxEdit' then
    
      Edit.render ( v, x, y, w, h )
    
    elseif v.type == 'dxGridList' then
    
      GridList.render( v, x, y, w, h )
    
    elseif v.type == 'dxSwitch' then
    
      Switch.render ( v, x, y, w, h )
    
    elseif v.type == 'dxImage' then
    
      Image.render ( v, x, y, w, h )
    
    elseif v.type == 'dxWindow' then
    
      Window.render ( v, x, y, w, h )
      
    elseif v.type == 'dxLine' then
    
      Line.render ( v, x, y )
     elseif v.type == 'dxCheckbox' then
    
      Checkbox.Render ( v, x, y, w, h )
    end
    
    dxSetRenderTarget()
    
    for k,v in ipairs ( v:getChildren() ) do
  
      if v.type:find('dx') then
      
        renderizarElemento ( v, x, y, w, h )
      
      end
  
    end
    
	end

end
