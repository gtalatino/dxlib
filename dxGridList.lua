-- ****************************************************************************
--
-- RECURSO: dxGrid
-- ARCHIVO: dxGrid.lua
-- PROPOSITO: Libreria de tablas DirectX
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

GridList = {}

-- element/dxGridList dxGridList ( int x, int y, int w, int h [, int color ] )
-- Crea un dxGridList
function dxGridList ( x, y, w, h, color )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
	assert( type( x ) == "number", "Mal argumento @ dxBox" );
	assert( type( y ) == "number", "Mal argumento @ dxBox" );
  assert( type( w ) == "number", "Mal argumento @ dxBox" );
	assert( type( h ) == "number", "Mal argumento @ dxBox" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxGridList' )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'w', w )
  self:setData ( 'h', h )
  self:setData ( 'visible', true )
  self:setData ( 'source', sourceResourceRoot )
  
  if color then

    -- Revisa si el valor 'color' fue entregado correctamente
    if type( color ) ~= 'number' then
  
      outputDebugString ( "Mal argumento 'color' @ dxBox", 2 )
  
    else
  
      self:setData ( 'color', color )
  
    end
    
  end
  
  self:setData ( 'table_data', {} )
  self:setData ( 'column_info', {} )
  self:setData ( 'bar_size', h )
  
  return self
  
end

-- int dxGridListAddColumn ( element/dxGridList self, string nombre [, int size ] )
-- Agrega una columna a la Gridlist entregada
function dxGridListAddColumn ( self, nombre, size )
  
  if not isElement( self ) or self.type ~= 'dxGridList' then
  
    outputDebugString ( "Mal argumento[1] @ dxGridListAddColumn", 1 )
    return false
  
  end
  
  if type( nombre ) ~= 'string' then
  
    outputDebugString ( "Mal argumento[2] @ dxGridListAddColumn", 1 )
    return false
  
  end
  
  local size = ( type( size ) == 'number' ) and size or ( dxGetTextWidth ( nombre ) + 10 ) / self:getData ('w')
  
  local info = self:getData('column_info')
  
  local xoffset = 0
  for k,v in ipairs ( info ) do
    
    xoffset = xoffset + v[2]
  
  end
  
  if xoffset + size > 1 then
  
    size = 1 - xoffset
    outputDebugString ( "El tamaño de la columna excede el ancho de tabla." , 2 )
  
  end
  
  
  local id = #info + 1
  info[ id ] = { nombre, size }
  self:setData ( 'column_info', info )
  
  return id
  
end

-- int dxGridListAddRow ( element/dxGridList self )
-- Agrega una fila a la GridList entregada.
function dxGridListAddRow ( self )

  if not isElement( self ) or self.type ~= 'dxGridList' then
  
    outputDebugString ( "Mal argumento[1] @ dxGridListAddRow", 1 )
    return false
  
  end
  
  local data = self:getData ( 'table_data' )
  local altura_visible = self:getData ( 'h' ) - 26
  local id = #data + 1
  data[ id ] = {}
  self:setData ( 'table_data', data )
  self:setData ( 'bar_size', math.min( altura_visible, math.ceil( altura_visible^2 / ( #data * 20 ) ) ) )
  
  return id

end

-- bool dxGridListSetItemText ( element/dxGridList self, int rowid, int columnid, string text )
-- Agrega un texto al espacio indicado en la gridlist entragada.
function dxGridListSetItemText ( self, rowid, columnid, text )

  if not isElement( self ) or self.type ~= 'dxGridList' then
  
    outputDebugString ( "Mal argumento[1] @ dxGridListSetItemText", 1 )
    return false
  
  end
  
  if type( rowid ) ~= 'number' then
  
    outputDebugString ( "Mal argumento[2] @ dxGridListSetItemText", 1 )
    return false
  
  end
  
  if type( columnid ) ~= 'number' then
  
    outputDebugString ( "Mal argumento[3] @ dxGridListSetItemText", 1 )
    return false
  
  end
  
  if type( text ) ~= 'string' then
  
    outputDebugString ( "Mal argumento[4] @ dxGridListSetItemText", 1 )
    return false
  
  end
  
  local data = self:getData ( 'table_data' )
  if not data[ rowid ] then
  
    outputDebugString ( "No existe la rowid entregada @ dxGridListSetItemText", 2 )
    return false
  
  end
  
  local info = self:getData ( 'column_info' )
  if not info[ columnid ] then
  
    outputDebugString ( "No existe la columnid entregada @ dxGridListSetItemText", 2 )
    return false
  
  end
  
  data[ rowid ][ columnid ] = text
  
  self:setData ( 'table_data', data )
  
  return true

end

-- bool dxGridListSetSelectedItem ( Element/dxGridList self, int rowid, int columnid )
-- Selecciona el item entregado de la lista.
function dxGridListSetSelectedItem ( self, rowid, columnid )

  if not isElement( self ) or self.type ~= 'dxGridList' then
  
    outputDebugString ( "Mal argumento[1] @ dxGridListSetSelectedItem", 1 )
    return false
  
  end
  
  if type( rowid ) ~= 'number' then
  
    outputDebugString ( "Mal argumento[2] @ dxGridListSetSelectedItem", 1 )
    return false
  
  end
  
  if type( columnid ) ~= 'number' then
  
    outputDebugString ( "Mal argumento[3] @ dxGridListSetSelectedItem", 1 )
    return false
  
  end

  local selected = self:setData ( 'selected', { columnid, rowid } )

  return true
  
end

-- int int dxGridListGetSelectedItem ( Element/dxGridList self )
-- Obtiene el item seleccionado de la lista.
function dxGridListGetSelectedItem ( self )

  if not isElement( self ) or self.type ~= 'dxGridList' then
  
    outputDebugString ( "Mal argumento[1] @ dxGridListSetSelectedItem", 1 )
    return false
  
  end

  local selected = self:getData ( 'selected' ) or { -1, -1 }

  return unpack ( selected )
  
end

-- void GridList.click ( element/dxGridList grid, float relx, float rely )
-- Maneja la selección de Items en la tabla.
function GridList.click ( grid, relx, rely )

  local info = grid:getData ( 'column_info' )
  local data = grid:getData ( 'table_data' )
  local bar_offset = grid:getData ( 'baroffset' ) or 0
  local h = grid:getData ( 'h' )
  
  local column_xoffset = 0
  local editor_offset = bar_offset * #data * 20 / ( h-26 ) or 0
  
  for k,v in ipairs( info ) do
  
    column_xoffset = column_xoffset + v[2]
    
    if column_xoffset > relx then
    
      for q,w in ipairs ( data ) do
      
        if ( 25 - editor_offset + 20 * q > rely * h ) then
        
          if ( 10 - editor_offset + 20 * q < rely * h ) and w[ k ] then
          
            grid:setData ( 'selected', {k,q} )
            return true
          
          else
          
            grid:setData ( 'selected', nil )
          
          end
        
        end        
      
      end
    
    end
  
  end
  
  return false

end

-- void GridList.render ( element/dxGridList grid )
-- Renderiza el gridlist
function GridList.render ( grid, x, y, w, h )

  -- Se obtienen los datos de la tabla
  local text = grid:getData ( 'text' )
  local color = grid:getData ( 'color' ) or 0xCC000000
  local info = grid:getData ( 'column_info' )
  local data = grid:getData ( 'table_data' )
  local selected = grid:getData ( 'selected' )
  local bar_offset = grid:getData ( 'baroffset' ) or 0
  local bar_size = grid:getData ( 'bar_size' )
  
  -- Se dibuja el fondo de esta.
  dxDrawRectangle ( x, y, w, h, color )
  dxDrawLine ( x + 3, y + 25, x + w - 10, y + 25, 0xAAFFFFFF )
  
  if #data * 20 > h - 25 then
  
    -- Dibuja la barra de desplazamiento
    dxDrawRectangle ( x + w - 10, y + 25, 10, h - 25, 0xAA222222)
    dxDrawRectangle ( x + w - 10, y + 25 + bar_offset, 10, bar_size, 0x77FFFFFF )
  
  end
  
  local editor_offset = bar_offset * #data * 20 / ( h-26 ) or 0
  
  -- Se marca la fila seleccionado si es que hay
  if selected and selected[2] > 0 then
    
    -- Se calcula la posición Y inicial previniendo que esta se ponga sobre el titulo de la columna.
    local ypos = ( 10 - editor_offset + 20 * selected[2] < 26) and y + 26 or y - editor_offset + 10 + 20 * selected[2]
    -- Previene que el rectangulo de selección se pose sobre la barra de desplazamiento.
    local dw = (#data * 20 > h - 25) and w - 10 or w
    -- Se calcula la altura del rectangulo previniendo que este salga de la lista o se sobreponga en el titulo de columna.
    local dh = ( 10 - editor_offset + 20 * selected[2] < 26 ) and - editor_offset + 20 * selected[2] or 15
    dh = ( 25 - editor_offset + 20 * selected[2] > h ) and h + editor_offset - 10 - 20 * selected[2] or dh
    
    -- Se revisa que efectivamente se muestre el rectangulo
    if dh > 0 then
    
      dxDrawRectangle ( x, ypos, dw, dh, 0x700000FF )
    
    end
    
  end
  
  -- Se itera por las columnas
  local column_xoffset = 0
  for k,v in ipairs ( info ) do
  
    -- Se muestra el nombre de la columna
    local right = column_xoffset + math.ceil(v[2] * w)
    dxDrawText ( v[1], x + column_xoffset + 5 , y + 5, x + right - 5, y + 25, nil, nil, 'default', nil, nil, true )
    
    for a,s in ipairs ( data ) do -- Itera por las filas
    
      if s[ k ] then -- Revisa que la columna de la tabla 'data' es la columna en la cual se esta trabajando ahora.
      
        -- Se calcula la posición Y inicial previniendo que esta se ponga sobre el titulo de la columna.
        local ypos = ( 10 - editor_offset + 20 * a < 26) and y + 26 or y - editor_offset + 10 + 20 * a
        -- Se calcula la posición Y final previniendo que esta salga de la lista.
        local dh = (25 - editor_offset + 20 * a > h - 1) and y + h - 1 or  y + 25 - editor_offset + 20 * a
        -- Se cambia la alineación en Y según la posición del dato.
        local yalign = (25 - editor_offset + 20 * a > h - 1) and 'top' or 'bottom'
        
        -- Revisa que el dato este efectivamente siendo mostrado en la lista.
        if not (15 - editor_offset + 20 * a > h - 1) and not ( dh < y + 26 ) then
        
          dxDrawText ( s[ k ], x + column_xoffset + 5 , ypos, x + right - 5, dh, nil, nil, 'default', nil, yalign, true )
        
        end
        
      end
    
    end
    
    column_xoffset = right
    
    if column_xoffset < w - 1 then 
    
      dxDrawLine ( x + column_xoffset, y + 3, x + column_xoffset, y + h - 3, 0x30FFFFFF ) -- Dibuja la linea de separación de columnas.
    
    end
  
  end
  
end

-- Llamado al mover la rueda del ratón
function GridList.Wheel( button )

  if button ~= 'mouse_wheel_up' and button ~= 'mouse_wheel_down' then return end -- Revisa que se este moviendo la rueda del ratón.
  
  local overElement = nil
  for k,v in ipairs ( getElementsByType( 'dxGridList', resourceRoot ) ) do -- Itera por las dxGridLists
  
    if isCursorInPosition ( v:getData('x'), v:getData('y'), v:getData('w'), v:getData('h') ) then -- Revisa que el cursor este en la GridList
      
      overElement = v
      break
      
    end
    
  end
  
  if overElement == nil then return end -- Si es que el cursor no esta sobre alguna gridlist, abortar.
  
  local offset = overElement:getData ( 'baroffset' ) or 0
  local bar_size = overElement:getData ( 'bar_size' )
  if button == 'mouse_wheel_down' then
  
    local h = overElement:getData('h')
    
    local nueva_posicion = offset + h / ( #overElement:getData('table_data') * 3 )
    local nuevo_offset = math.min ( nueva_posicion, h - 26 - bar_size  )
    
    if DEBUG then
      outputDebugString ( 'h='..h..' | offset='..offset..' | nuevo_offset='..nuevo_offset )
    end
    
    overElement:setData ( 'baroffset', nuevo_offset )
  
  else
  
    if offset == 0 then return end
    
    -- Se calcula la cantidad a quitar teniendo en cuenta que no debe salir de posición.
    local take = ( offset - 8 < 0 ) and offset or 8
    overElement:setData ( 'baroffset', offset - take )
  
  end

end
addEventHandler ( 'onClientKey', root, GridList.Wheel )