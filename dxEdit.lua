-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxEdit.lua
-- PROPOSITO: Libreria de Campo de edición
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--            Federico Romero < TheCrazy >
--
-- ****************************************************************************

addEvent ( 'dxEditChange' )

Edit = {}

-- element/dxEdit dxEdit ( int x, int y, int w, int h, string text )
-- Crea una caja de edición de texto bajo los argumentos dados
function dxEdit ( x, y, w, h, text, postgui )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
	assert( type( x ) == "number", "Mal argumento @ dxEdit" );
	assert( type( y ) == "number", "Mal argumento @ dxEdit" );
  assert( type( w ) == "number", "Mal argumento @ dxEdit" );
	assert( type( h ) == "number", "Mal argumento @ dxEdit" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxEdit' )
  
  -- Si es que no se logro crear el elemento, devolver 'false' por el fallo e informar.
  if not self then
  
    outputDebugString( 'No se pudo crear el elemento de dxEdit', 2 )
    return false
  
  end
  
  if text then

    -- Revisa si el valor 'text' fue entregado correctamente
    if type( text ) ~= 'string' then
  
      outputDebugString ( "Mal argumento 'text' @ dxEdit", 2 )
  
    end
  
  else
  
    text = ''
    
  end
  
  if postgui then

    -- Revisa si el valor 'text' fue entregado correctamente
    if type( postgui ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'postgui' @ dxEdit", 2 )
  
    end
  
  else
  
    postgui = false
    
  end
  
  -- Agrega los datos requeridos al elemento}
  self:setData ( 'source', sourceResourceRoot )
  self:setData ( 'visible', true )
  self:setData ( 'text', text )
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'w', w )
  self:setData ( 'h', h )
  self:setData ( 'postgui', postgui )
  
  -- Devuelve el elemento ya codificado.
  return self
  
end

lastCaret = 0 -- Ultimo caret
caretPos = 0 -- Posición del caret
lastKey = 0 -- Tiempo de la ultima llave presionada

-- void dxEditCharacter ( string key )
-- Función que se llama cada vez que una tecla se presione
function dxEditCharacter ( key )

  if not isElement( e_enfoque ) or e_enfoque.type ~= 'dxEdit'  then return end
  
  local text = e_enfoque:getData ( 'text' ) -- Se obtiene el texto.
  local max_length = e_enfoque:getData ( 'max_length' )
  
  local new_text = utf8.sub ( text, 1, caretPos ) .. key .. utf8.sub ( text, caretPos + 1 ) -- Se codifica el texto con el nuevo caracter.
  
  if max_length and utf8.len(new_text) > max_length then
    
    return false
  
  end
  
  triggerEvent ( 'dxEditChange', e_enfoque, new_text )
  
  -- Si es que el evento no fue cancelado, entonces procede a mostrar los cambios.
  if not wasEventCancelled () then
  
    e_enfoque:setData ( 'text', new_text )
    caretPos = caretPos + 1
  
  end

end
addEventHandler ( 'onClientCharacter', root, dxEditCharacter )

-- bool setFocusedEdit ( element/dxEdit elemento )
-- Pone en enfoque el dxEdit entregado.
function setFocusedEdit ( elemento, bool )

  if not isElement( elemento ) or elemento.type ~= 'dxEdit' then
  
    outputDebugString ( "Mal argumento @ setFocusedEdit", 2 )
    return false
  
  end
	if bool then
		e_enfoque = elemento
	else
		e_enfoque = nil
	end
  return true

end

-- bool Edit.render ( element/dxEdit self, int x, int y, int w, int h )
-- Renderiza la caja de edición de texto
function Edit.render ( self, x, y, w, h )

  local tick = getTickCount ()
  local text = self:getData ( 'text' )
  local mask = self:getData ( 'mask' )
  local postgui = self:getData ( 'postgui' )
  local color_borde = self:getData ( 'color_borde' ) or 0x99EEEEEE
  local color_fondo = self:getData ( 'color_fondo' ) or 0xBB000000
  local color_texto = self:getData ( 'color_texto' ) or 0xFFFFFFFF
  local color_caret = self:getData ( 'color_caret' ) or 0xFF6666FF
  local fuente = self:getData ( 'fuente' ) or 'default'
  local aligny = self:getData ( 'aligny' ) or 'center'
  local wordbreak = self:getData ( 'wordbreak' ) or false
  
  if mask then
  
    text = text:gsub('.','*')
  
  end
  
  local y_offset = ( aligny == 'center' ) and 0 or 4 
  
  dxDrawRectangle ( x, y, w, h, color_borde, postgui )
  dxDrawRectangle ( x+2, y+2, w-4, h-4, color_fondo, postgui )
  dxDrawText ( text, x + 6, y + y_offset, w + x - 12, h + y - y_offset, color_texto, 1, fuente, 'left', aligny, true, wordbreak, postgui )
  
  
  
  if e_enfoque == self then
  
    if tick - lastKey > 100 then
  
      if getKeyState ( 'arrow_r' ) then
  
        caretPos = caretPos + 1
        lastKey = tick
        lastCaret = tick - 500
  
      elseif getKeyState ( 'arrow_l' ) and caretPos > 0 then
  
        caretPos = caretPos - 1
        lastKey = tick
        lastCaret = tick - 500
  
      elseif getKeyState ( 'backspace' ) and caretPos > 0 then
      
        caretPos = caretPos - 1
        lastKey = tick
        lastCaret = tick - 500
        
        local new_text = utf8.sub( text, 1, caretPos ) .. utf8.sub( text, caretPos + 2 )
        self:setData ( 'text' , new_text )
        
        triggerEvent ( 'dxEditChange', e_enfoque, new_text )
        
      elseif getKeyState ( 'delete' ) and caretPos < utf8.len ( text ) then
      
        lastKey = tick
        lastCaret = tick - 500
        
        local new_text = utf8.sub( text, 1, caretPos ) .. utf8.sub( text, caretPos + 2 )
        self:setData ( 'text' , new_text )
        
        triggerEvent ( 'dxEditChange', e_enfoque, new_text )
      
      elseif getKeyState ( 'enter' ) and wordbreak then
      
        lastKey = tick
        lastCaret = tick - 500
        
        local new_text = utf8.sub( text, 1, caretPos ) .. '\n' .. utf8.sub( text, caretPos + 1 )
        self:setData ( 'text' , new_text )
        
        caretPos = caretPos + 1
        
        triggerEvent ( 'dxEditChange', e_enfoque, new_text )
      end
  
    end
    
    if tick - lastCaret > 500 then
    
      if tick - lastCaret > 1000 then
      
        lastCaret = tick
      
      end
      
      if caretPos > utf8.len( text ) then caretPos = utf8.len( text ) end
    
      local fontHeight = dxGetFontHeight ( 1, fuente )
      local caretOffset = dxGetTextWidth ( utf8.sub( text, 1, caretPos ), 1, fuente )
      local starty = ( aligny == 'center' ) and h/2 - fontHeight/2 or y_offset
      local wb_y = 0
      
      if wordbreak then
      
        wb_y,caretOffset = getTextHeight(text,w,caretPos,fuente)
      
      end
      
      dxDrawRectangle ( x + 6 + caretOffset, y + starty + wb_y, 1, fontHeight, color_caret, postgui )
  
    end
  
  end

end