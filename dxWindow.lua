-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxWindow.lua
-- PROPOSITO: Libreria de Ventanas
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

Window = {}

function dxWindow ( x, y, w, h, titulo )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
	assert( type( x ) == "number", "Mal argumento @ dxBox" );
	assert( type( y ) == "number", "Mal argumento @ dxBox" );
  assert( type( w ) == "number", "Mal argumento @ dxBox" );
	assert( type( h ) == "number", "Mal argumento @ dxBox" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxWindow' )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'w', w )
  self:setData ( 'h', h )
  self:setData ( 'visible', true )
  self:setData ( 'source', sourceResourceRoot )
  
  if type( titulo ) ~= 'string' then

    if titulo then
    
      outputDebugString ( "Mal argumento 'titulo' @ dxWindow", 2 )
    
    end
    
    titulo = ''
    
  end
  
  self:setData ( 'titulo', titulo )
  
  return self
  
end

function Window.click ()

  

end

function Window.render ( v, x, y, w, h )

  local titulo = v:getData ( 'titulo' )
  
  dxDrawRectangle ( x, y, w, 20, 0xDD000000 )
  dxDrawRectangle ( x, y + 20, w, h - 20, 0x88000000 )
  dxDrawText ( titulo, x, y, x + w, y + 20, nil, 1, 'default', 'center', 'center' )

end
