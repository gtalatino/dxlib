-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxSwitch.lua
-- PROPOSITO: Librera de interruptor DirectX
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

Switch = {}

-- element/dxSwitch dxSwitch ( int x, int y, int w, int h, bool state, float font_size )
-- Crea un switch DirectX
function dxSwitch ( x, y, w, h, state, font_size )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
	assert( type( x ) == "number", "Mal argumento [1] @ dxSwitch" );
	assert( type( y ) == "number", "Mal argumento [2] @ dxSwitch" );
  assert( type( w ) == "number", "Mal argumento [3] @ dxSwitch" );
	assert( type( h ) == "number", "Mal argumento [4] @ dxSwitch" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxSwitch' )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'w', w )
  self:setData ( 'h', h )
  self:setData ( 'visible', true )
  self:setData ( 'source', sourceResourceRoot )

  if state then

    -- Revisa si el valor 'color' fue entregado correctamente
    if type( state ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento[5] @ dxText", 2 )
  
    else
  
      self:setData ( 'state', state )
  
    end
    
  end
  
	if not font_size then
  
		font_size = math.min( ( h - 10 ) / 15 , w / 48 )
    
	end
  
  self:setData ( 'font_size', font_size )

	return self
end

-- bool dxSwitchSetState ( element/dxSwitch self, bool state )
-- Cambia el estado del switch.
function dxSwitchSetState ( self, state )

  if not isElement( self ) or self.type ~= 'dxSwitch' then
  
    outputDebugString ( "Mal argumento[1] @ dxSwitchSetState", 1 )
    return false
  
  end
  
  if type( state ) ~= 'boolean' then
  
    outputDebugString ( "Mal argumento[2] @ dxSwitchSetState", 1 )
    return false
  
  end
  
  self:setData ( 'state', state )
  
  return true

end

Switch.change_time = 250 -- Tiempo en que se cambia el estado ( en milisegundos )
-- bool Switch.render ( element/dxSwitch self )
-- Renderiza el switch
function Switch.render ( self, x, y, w, h )

  local state = self:getData ( 'state' ) or false
  local last = self:getData ( 'last' )
  local tick = self:getData ( 'tick' )
  local font_size = self:getData('font_size')
  
  if last ~= state then
  
    local actual_tick = getTickCount()
    tick = ( tick ) and 2 * actual_tick - tick - Switch.change_time or actual_tick
    self:setData( 'tick', tick )
  
  end
  
  local white = 0xFFFFFFFF
  local font = 'default'
  local color = 0xFF550000
  local pos = ( state ) and math.ceil( w / 2 ) or 0
  local color = ( state ) and 0xFF006600 or 0xFF660000
  
  if tick then
  
    local actual_tick = getTickCount()
    if state then
    
      if Switch.change_time + tick > actual_tick then 
        
        local prog = (actual_tick - tick) / Switch.change_time
        pos = math.ceil( prog * w / 2 )
        color = tocolor ( 102 - prog * 102, prog * 102, 0, 255 )
      
      else
      
        self:setData ( 'tick', nil )
      
      end
    
    else
    
      if Switch.change_time + tick > actual_tick then 
        
        local prog = (actual_tick - tick) / Switch.change_time
        pos = math.ceil( w/2 - prog * w / 2 )
        color = tocolor ( prog * 102, 102 - prog * 102, 0, 255 )
      
      else
      
        self:setData ( 'tick', nil )
      
      end
    
    end
    
  end
  
  dxDrawRectangle ( x, y, w, h, color )
  
	dxDrawText ( 'ON', x, y, x+w/2, y + h, white, font_size, font, 'center', 'center' )
	dxDrawText ( 'OFF', x+w/2, y, x+w, y + h, white, font_size, font, 'center', 'center' )
  
  dxDrawRectangle ( x + pos, y, w/2, h, 0xFF777777 )
  
  self:setData ( 'last', state )
  
end
