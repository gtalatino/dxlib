-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxText.lua
-- PROPOSITO: Libreria de Texto
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

Text = {}

function dxText ( texto, x, y, w, h, color, scale, font, alignx, aligny, 
    clip, wordbreak, postgui, colorcoded, subpixel, rot, rotcenterx, rotcentery )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
  assert( type( texto ) == "string", "Mal argumento @ dxText" );
	assert( type( x ) == "number", "Mal argumento @ dxText" );
	assert( type( y ) == "number", "Mal argumento @ dxText" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxText' )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'text', texto )
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'visible', true )
  self:setData ( 'source', sourceResourceRoot )
  
  if w then
    
    -- Revisa si el valor 'w' fue entregado correctamente
    if type( w ) ~= 'number' then
    
      outputDebugString ( "Mal argumento 'w' @ dxText", 2 )
  
    else
  
      self:setData ( 'w', w )
  
    end
  
  end
  
  if h then

    -- Revisa si el valor 'h' fue entregado correctamente
    if type ( h ) ~= 'number' then
  
      outputDebugString ( "Mal argumento 'h' @ dxText", 2 )
  
    else
  
      self:setData ( 'h', h )
    
    end
  
  end
  
  if color then

    -- Revisa si el valor 'color' fue entregado correctamente
    if type( color ) ~= 'number' then
  
      outputDebugString ( "Mal argumento 'color' @ dxText", 2 )
  
    else
  
      self:setData ( 'color', color )
  
    end
    
  end
  
  if scale then
    
    if type ( scale ) ~= 'number' then
  
      outputDebugString ( "Mal argumento 'scale' @ dxText", 2 )
  
    else
  
      self:setData ( 'scale', scale )
    
    end
  
  end
  
  if font then
  
    if type ( font ) ~= 'string' and type ( font ) ~= 'userdata' then
  
      outputDebugString ( "Mal argumento 'font' @ dxText", 2 )
  
    else
  
      self:setData ( 'font', font )
    
    end
  
  end
  
  if alignx then

    if type ( alignx ) ~= 'string' then
  
      outputDebugString ( "Mal argumento 'alignx' @ dxText", 2 )
  
    else
  
      self:setData ( 'alignx', alignx )
    
    end
  
  end
  
  if aligny then

    if type ( aligny ) ~= 'string' then
  
      outputDebugString ( "Mal argumento 'aligny' @ dxText", 2 )
  
    else
  
      self:setData ( 'aligny', aligny )
    
    end
  
  end
  
  if clip then

    if type ( clip ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'scale' @ dxText", 2 )
  
    else
  
      self:setData ( 'clip', clip )
  
    end
  
  end
  
  if wordbreak then

    if type ( wordbreak ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'wordbreak' @ dxText", 2 )
  
    else
  
      self:setData ( 'wordbreak', wordbreak )
  
    end
  
  end
  
  if postgui then

    if type ( postgui ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'postgui' @ dxText", 2 )
  
    else
  
      self:setData ( 'postgui', postgui )
  
    end
  
  end
  
  if colorcoded then

    if type ( colorcoded ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'colorcoded' @ dxText", 2 )
  
    else
  
      self:setData ( 'colorcoded', colorcoded )
  
    end
  
  end
  
  if subpixel then

    if type ( subpixel ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'subpixel' @ dxText", 2 )
  
    else
  
      self:setData ( 'subpixel', subpixel )
  
    end
  
  end
  
  if rot then

    if type ( rot ) ~= 'number' then
  
      outputDebugString ( "Mal argumento 'rot' @ dxText", 2 )
  
    else
  
      self:setData ( 'rot', rot )
  
    end
  
  end
  
  if rotcenterx then

    if type ( rotcenterx ) ~= 'number' then
  
      outputDebugString ( "Mal argumento 'rotcenterx' @ dxText", 2 )
  
    else
  
      self:setData ( 'rotcenterx', rotcenterx )
  
    end
  
  end
  
  if rotcentery then

    if type ( rotcentery ) ~= 'number' then
  
      outputDebugString ( "Mal argumento 'rotcentery' @ dxText", 2 )
  
    else
  
      self:setData ( 'rotcentery', rotcentery )
  
    end
  
  end
  
  return self
  
end

function Text.render ( v, x, y, w, h )

  local text = v:getData ( 'text' )
  local color = v:getData ( 'color' ) or nil
  local scale = v:getData ( 'scale' ) or nil
  local font = v:getData ( 'font' ) or 'default'
  local alignx = v:getData ( 'alignx' ) or nil
  local aligny = v:getData ( 'aligny' ) or nil
  local clip = v:getData ( 'clip' ) or nil
  local wordbreak = v:getData ( 'wordbreak' ) or nil
  local postgui = v:getData ( 'postgui' ) or nil
  local colorcoded = v:getData ( 'colorcoded' ) or nil
  local subpixel = v:getData ( 'subpixel' ) or nil
  local rot = v:getData ( 'rot' ) or nil
  local rotcenterx = v:getData ( 'rotcenterx' ) or nil
  local rotcentery = v:getData ( 'rotcentery' ) or nil
  
  -- Si es que se define, produce un texto con bordes.
  local outline = v:getData ( 'outline' )
  if outline and type ( outline ) == 'number' then
    
		for offsetX=-outline,outline,outline do
			
      for offsetY=-outline,outline,outline do
				
        if not (offsetX == 0 and offsetY == 0) then
				
          dxDrawText ( text, x + offsetX, y + offsetY, x + w, y + h, 0xFF000000, scale, font, alignx, aligny, clip, wordbreak, postgui, colorcoded, subpixel, rot, rotcenterx, rotcentery )
			
        end
		
      end
		
    end
  
  end
  
  dxDrawText ( text, x, y, x + w, y + h, color, scale, font, alignx, aligny, clip, wordbreak, postgui, colorcoded, subpixel, rot, rotcenterx, rotcentery )

end
