-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxImage.lua
-- PROPOSITO: Libreria de imagen
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

Image = {}

function dxImage ( x, y, w, h, image, color, postgui )

  -- Revisa que los argumentos requeridos se hayan entregado correctamente
	assert( type( x ) == "number", "Mal argumento @ dxImage" );
	assert( type( y ) == "number", "Mal argumento @ dxImage" );
  assert( type( w ) == "number", "Mal argumento @ dxImage" );
	assert( type( h ) == "number", "Mal argumento @ dxImage" );
  assert( type( image ) == "string" or ( isElement(image) and ( image.type == 'texture' or image.type == 'shader' ) ), "Mal argumento @ dxImage" );
	
  -- Crea el elemento de texto
  local self = Element ( 'dxImage' )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'x', x )
  self:setData ( 'y', y )
  self:setData ( 'w', w )
  self:setData ( 'h', h )
  self:setData ( 'image', image )
  self:setData ( 'visible', true )
  self:setData ( 'source', sourceResourceRoot )
  
  if color then

    -- Revisa si el valor 'color' fue entregado correctamente
    if type( color ) ~= 'number' and type( color ) ~= 'string' then
  
      outputDebugString ( "Mal argumento 'color' @ dxImage", 2 )
  
    else
  
      self:setData ( 'color', color )
  
    end
    
  end
  
  if postgui then

    -- Revisa si el valor 'text' fue entregado correctamente
    if type( postgui ) ~= 'boolean' then
  
      outputDebugString ( "Mal argumento 'postgui' @ dxImage", 2 )

  
    else
  
      self:setData ( 'postgui', postgui )
    
    end
    
  end
  
  -- Actualización de dxTarget si existe.
  if ( isElement(image) and ( image.type == 'texture' ) ) then
  
    for k,v in ipairs ( getElementsByType('texture', resourceRoot ) ) do
      if v == image then
        v:setData ( 'x', x )
        v:setData ( 'y', y )
        break
      
      end
      
    end
  
  end
  
  return self
  
end

function Image.render ( v, x, y, w, h )

  local image = v:getData ( 'image' )
  local color = v:getData ( 'color' ) or nil
  local postgui = v:getData ('postgui')
  
  dxDrawImage ( x, y, w, h, image, 0, 0, 0, color, postgui )

end
