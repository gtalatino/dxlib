-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: dxRenderTarget.lua
-- PROPOSITO: Libreria de RT
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

RenderTarget = {}

function dxRenderTarget ( w, h, alpha )

  -- Revisa que las funciones requeridas se hayan entregado correctamente
	assert( type( w ) == "number", "Mal argumento w @ dxRenderTarget" );
	assert( type( h ) == "number", "Mal argumento h @ dxRenderTarget" );
  
  alpha = ( alpha ) and alpha or false
	
  -- Crea el elemento de texto
  local self = dxCreateRenderTarget ( w, h, alpha )
  
  if not self then return false end
  
  -- Agrega los datos requeridos al elemento
  self:setData ( 'w', w )
  self:setData ( 'h', h )
  self:setData ( 'source', sourceResourceRoot )
  
  return self
  
end