-- ****************************************************************************
--
-- RECURSO: dxLib
-- ARCHIVO: client.lua
-- PROPOSITO: Codigo de cliente
-- CREADORES: Stefano Aguilera Humeney < Gothem >
--
-- ****************************************************************************

DYNAMICROOT = getResourceDynamicElementRoot ( resource )
caretShowing = false
e_enfoque = nil

addEvent ( 'dxClick' )
addEvent ( 'dxCursorEnter' )
addEvent ( 'dxCursorLeave' )
addEvent ( 'dxButtonClick' ) -- CODIGO PARA COMPATIBILIDAD

function isCursorInPosition(x, y, w, h, dxtarget)
	if not isCursorShowing () then return false end

	local sx, sy = guiGetScreenSize ( )
	local cx, cy = getCursorPosition ( )
	local cx, cy = ( cx * sx ), ( cy * sy )
  
  if dxtarget then
  
    local dx = dxtarget:getData ( 'x' ) or 0
    local dy = dxtarget:getData ( 'y' ) or 0
    local dw = dxtarget:getData ( 'w' ) or w
    local dh = dxtarget:getData ( 'h' ) or h
    
    w = ( x + w > dw ) and dw - x or w
    h = ( y + h > dh ) and dh - y or h
    x = x + dx
    y = y + dy
  
  end
  
	if ( cx > x and cx < x + w ) and ( cy > y and cy < y + h ) then
		return true
	else
		return false
	end
end

function getTextHeight ( text, w, endpos, fuente, scale )

  -- DEFAULT
  endpos = endpos or utf8.len(text)
  fuente = fuente or 'default'
  scale = scale or 1

  local fontHeight = dxGetFontHeight ( scale, fuente )
  local end_x,y = 0,0
  local last_t_pos = 1
  local last_fixed_pos = 0
  local fixed = false
  
  -- LOGIC
  for i=1, endpos do
  
    end_x = dxGetTextWidth ( utf8.sub( text, last_t_pos, i ), scale, fuente )
    local wb = ( math.floor( end_x / (w-17) ) == 1 )
    
    -- ENTER
    if utf8.byte( text, i, i ) == 10 then
    
      last_t_pos = i
      end_x = 0
      y = y + fontHeight - 1
      fixed = false
    
    end
    
    if wb then
    
      last_t_pos = ( fixed ) and last_fixed_pos or i
      end_x = 0
      y = y + fontHeight - 1
      fixed = false
    
    end
    
    -- ESPACIO
    if utf8.byte( text, i, i ) == 32 or utf8.byte( text, i, i ) == 44 or utf8.byte( text, i, i ) == 46 then
    
      fixed = true
      last_fixed_pos = i+1
    
    end
  
  end
  
  return y,end_x

end

function clickCliente ( button, state, cx, cy )

  if state ~= 'down' then return end
  
  for k,v in ipairs( getElementChildren ( DYNAMICROOT ) ) do
  
    if isElement(v) and v.type:find('dx') then
    
      clickElemento ( v, button, cx, cy )
    
    end
  
  end


end
addEventHandler ( 'onClientClick', root, clickCliente )

function clickElemento ( elemento, button, cx, cy, px, py )
	if not isElement(elemento) or elemento:getData ('disabled') then return end

	local x = elemento:getData ( 'x' ) + ( px or 0 )
	local y = elemento:getData ( 'y' ) + ( py or 0 )
	local w = elemento:getData ( 'w' ) or 0
	local h = elemento:getData ( 'h' ) or 0
  
  local dxtarget = elemento:getData ( 'dxtarget' )

  if dxtarget then
  
    local dx = dxtarget:getData ( 'x' ) or 0
    local dy = dxtarget:getData ( 'y' ) or 0
    local dw = dxtarget:getData ( 'w' ) or w
    local dh = dxtarget:getData ( 'h' ) or h
    
    w = ( x + w > dw ) and dw - x or w
    h = ( y + h > dh ) and dh - y or h
    x = x + dx
    y = y + dy
  
  end
  
	if cx < x or cx > x + w or cy < y or cy > y + h then
		if e_enfoque == elemento then
    
      if e_enfoque.type == 'dxEdit' then
      
        guiSetInputMode ( 'allow_binds' )
      
      end
    
			e_enfoque = nil
		end
		return
	end

	triggerEvent ( 'dxClick', elemento, button )

	if wasEventCancelled () or not isElement ( elemento ) then return end
  
	-- CODIGO PARA COMPATIBILIDAD
	if elemento.type == 'dxButton' then
		triggerEvent ( 'dxButtonClick', elemento, button )
	elseif elemento.type == 'dxSwitch' or elemento.type == 'dxCheckbox' and button == 'left' then
		elemento:setData ( 'state', not elemento:getData ( 'state' ) )
	elseif elemento.type == 'dxGridList' then
		GridList.click ( elemento, ( cx - x ) / w, ( cy - y ) / h )
  elseif elemento.type == 'dxEdit' then
    guiSetInputMode ( 'no_binds' )
	end

	e_enfoque = elemento

	if isElement(elemento) and elemento:getChildren() then
		for k,v in ipairs(elemento:getChildren()) do
			if isElement(v) and v.type:find('dx') then
				clickElemento ( v, button, cx, cy, x, y )
			end
		end
	end
end

function h_dxCursorMove ( _, _, cx, cy )
  
  if isMainMenuActive () then return end

  for k,v in ipairs( getElementChildren ( DYNAMICROOT ) ) do
  
    if isElement(v) and v.type:find('dx') then
    
      dxCursorMove ( v, cx, cy )
    
    end
  
  end

end
addEventHandler ( 'onClientCursorMove', root, h_dxCursorMove )

function dxCursorMove ( elemento, cx, cy, px, py )

  if not isElement(elemento) or elemento:getData ('disabled') then return end
  
  local x = elemento:getData ( 'x' ) + ( px or 0 )
  local y = elemento:getData ( 'y' ) + ( py or 0 )
  local w = elemento:getData ( 'w' ) or 0
  local h = elemento:getData ( 'h' ) or 0
  
  local dxtarget = elemento:getData ( 'dxtarget' )

  if dxtarget then
  
    local dx = dxtarget:getData ( 'x' ) or 0
    local dy = dxtarget:getData ( 'y' ) or 0
    local dw = dxtarget:getData ( 'w' ) or w
    local dh = dxtarget:getData ( 'h' ) or h
    
    w = ( x + w > dw ) and dw - x or w
    h = ( y + h > dh ) and dh - y or h
    x = x + dx
    y = y + dy
  
  end
  
  if ( cx >= x and cx < x + w ) and ( cy >= y and cy <= y + h ) then
  
    if not elemento:getData ( 'cursorInside' ) then
    
      elemento:setData( 'cursorInside', true )
      triggerEvent ( 'dxCursorEnter', elemento, cx, cy )
    
    end
  
  elseif elemento:getData( 'cursorInside') then
  
    elemento:setData( 'cursorInside', false )
    triggerEvent ( 'dxCursorLeave', elemento, cx, cy )
  
  end
  
  for k,v in ipairs ( elemento:getChildren() ) do
  
    if v.type:find('dx') then
    
      dxCursorMove ( v, cx, cy, x, y )
    
    end
  
  end

end

function onResourceStop ( res )

  local tipos = { 'texture', 'dxText', 'dxBox', 'dxButton', 'dxEdit', 'dxSwitch', 'dxGridList', 'dxImage', 'dxWindow', 'dxLine', 'dxCheckbox' }

  for k,v in ipairs( tipos ) do
  
    for q,w in ipairs( getElementsByType ( v, resourceRoot ) ) do

      if w:getData ( 'source' ) == source then
      
        w:destroy ()
      
      end
  
    end
  
  end

end
addEventHandler ( 'onClientResourceStop', root, onResourceStop )

function Elemento_destruido ()

  if e_enfoque == source then
    
    e_enfoque = nil
    
  end

end
addEventHandler ( 'onElementDestroy', resourceRoot, Elemento_destruido )